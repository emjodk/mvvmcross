﻿using MvvmCross.ViewModels;
using PictureApp.Core.ViewModels;

namespace PictureApp.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();
            RegisterAppStart<PictureViewModel>();
        }
    }
}
