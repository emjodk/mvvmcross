﻿using MvvmCross.Commands;
using MvvmCross.Plugin.PictureChooser;
using MvvmCross.ViewModels;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;

namespace PictureApp.Core.ViewModels
{
    public class PictureViewModel : MvxViewModel
    {
        private readonly IMvxPictureChooserTask _pictureChooserTask;
        private MvxCommand _takePictureCommand;
        private MvxCommand _choosePictureCommand;

        public PictureViewModel(IMvxPictureChooserTask pictureChooserTask)
        {
            _pictureChooserTask = pictureChooserTask;
        }

        public ICommand TakePictureCommand
        {
            get
            {
                _takePictureCommand = _takePictureCommand ?? new MvxCommand(DoTakePicture);
                return _takePictureCommand;
            }
        }

        public void DoTakePicture()
        {
            _pictureChooserTask.TakePicture(640, 40, OnPicture, () => { });
        }


        public ICommand ChoosePictureCommand
        {
            get
            {
                _choosePictureCommand = _choosePictureCommand ?? new MvxCommand(DoChoosePicture);
                return _choosePictureCommand;
            }
        }

        public void DoChoosePicture()
        {
            _pictureChooserTask.ChoosePictureFromLibrary(640, 50, OnPicture, () => { });
        }

        public void OnPicture(Stream stream)
        {
            Debug.WriteLine("Picture happened!?!");
        }
    }
}
