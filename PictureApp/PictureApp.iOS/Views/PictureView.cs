﻿
using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using PictureApp.Core.ViewModels;
using UIKit;

namespace Blank.Views
{
    public partial class PictureView : MvxViewController<PictureViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;
            View.BackgroundColor = UIColor.White;

            UILabel label;
            Add(label = new UILabel());
            label.Text = "Camera app";
            label.Font = UIFont.FromName("Verdana-Bold", 12);

            UIButton pictureButton;
            Add(pictureButton = new UIButton(UIButtonType.RoundedRect));
            pictureButton.SetTitle("Take Picture", UIControlState.Normal);
            pictureButton.LayoutMargins = UIEdgeInsets.FromString("29");
            pictureButton.Font = UIFont.FromName("Verdana-Bold", 18);

            UIButton choosePictureButton;
            Add(choosePictureButton = new UIButton(UIButtonType.RoundedRect));
            choosePictureButton.SetTitle("Choose Picture", UIControlState.Normal);
            choosePictureButton.LayoutMargins = UIEdgeInsets.FromString("39");
            choosePictureButton.Font = UIFont.FromName("Verdana-Bold", 18);

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            View.AddConstraints(
               label.AtTopOf(View, 18),
               label.WithSameCenterX(View),

               pictureButton.Below(label, 20),
               pictureButton.WithSameLeft(label),

               choosePictureButton.Below(pictureButton, 30),
               choosePictureButton.WithSameLeft(pictureButton)
            );

            var set = this.CreateBindingSet<PictureView, PictureViewModel>();
            set.Bind(pictureButton).For("TouchUpInside").To(vm => vm.TakePictureCommand);
            set.Bind(choosePictureButton).For("TouchUpInside").To(vm => vm.ChoosePictureCommand);
            set.Apply();

            pictureButton.BecomeFirstResponder();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }
    }
}